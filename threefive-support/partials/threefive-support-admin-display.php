<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<form action="" id="threefive-support" enctype="multipart/form-data">
	<label for="tf_your_email">
		<input type="text" name="tf_your_email" id="tf_your_email" placeholder="Email">
	</label>
	
	<label for="tf_your_name">
		<input type="text" name="tf_your_name" id="tf_your_name" placeholder="Name">
	</label>
	
	<label for="tf_support_request_msg">
		<textarea name="tf_support_request_msg" id="tf_support_request_msg" cols="30" rows="10" placeholder="What can we help you with?"></textarea>
	</label>

	<input type="submit" name="tf_support_submit" id="tf_support_submit" class="button button-primary button-hero" value="Get Support">
</form>