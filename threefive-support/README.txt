=== Plugin Name ===
Contributors: 3five, VincentListrani
Donate link: 
Tags: support, wordpress support, theme support, helpdesk
Requires at least: 4.1.1
Tested up to: 4.1.1
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Submit a support request ticket to Support@3five directly from your dashboard.

== Description ==

Submit a support request ticket to Support@3five directly from your dashboard. If you've found a bug with your custom 3five developed WordPress theme, would like to modify a feature, or  use this form to submit and generate a ticket with our support team. 

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `plugin-name.php` to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress
1. Place `<?php do_action('plugin_name_hook'); ?>` in your templates

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.1 =
* Initial Release.

== Upgrade Notice ==

